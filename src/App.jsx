import {
  AppBar,
  Button,
  ButtonGroup,
  Toolbar,
  Tooltip,
  Typography,
} from "@mui/material"
import { Handyman } from "@mui/icons-material"
import React, { useEffect, useState } from "react"
import styled from "styled-components"
import Calculator from "./components/Calculator"

const Logo = styled(Typography).attrs(() => ({
  variant: "h1",
}))`
  font-size: 30px;
  display: flex;
  align-items: center;
  gap: 10px;
  margin: 0 auto;

  svg {
    width: 1.3em;
    height: 1.3em;
  }
`

const App = () => {
  return (
    <React.Fragment>
      <AppBar position="static">
        <Toolbar>
          <Logo variant="h1">
            <Handyman /> Nomifactory/Omnifactory overclock calculator
          </Logo>
        </Toolbar>
      </AppBar>
      <Calculator />
    </React.Fragment>
  )
}

export default App
