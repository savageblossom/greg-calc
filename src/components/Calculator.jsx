import React, { useEffect, useMemo, useState } from "react"
import styled from "styled-components"
import voltageTiers from "@/assets/voltageTiers.json"
import {
  Box,
  Button,
  ButtonGroup,
  Chip,
  Container,
  Divider,
  Grid,
  InputAdornment,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  TextField,
  Tooltip,
  Typography,
} from "@mui/material"

import { ElectricBolt, AvTimer, Error as ErrorIcon } from "@mui/icons-material"

import debounce from "lodash.debounce"

const VoltageButtonGroup = styled(ButtonGroup)`
  display: flex;
  width: 100%;
  margin-top: 10px;
`
const VoltageButton = styled(Button)`
  background: ${(props) => (props.isSelected ? "#1976d2" : "initial")};
  color: ${(props) => (props.isSelected ? "white" : "initial")};
  opacity: 0.8;
  flex: 1 0 auto;
  padding: 0;

  &:hover {
    background: #1976d2;
    color: white;
    opacity: 0.6;
  }

  p {
    width: 100%;
    height: 40px;
    display: flex;
    justify-content: center;
    align-items: center;
  }
`
const StyledResult = styled(Typography)`
  display: flex;
  gap: 5px;
`

const Error = styled(Typography).attrs(() => ({ variant: "body2" }))`
  color: red;
  opacity: 0.6;
  margin-top: 15px;
`

const ResultRowContent = ({ icon, name }) => (
  <StyledResult>
    {icon}
    {name}
  </StyledResult>
)

const Calculator = () => {
  const [selectedTier, setSelectedTier] = useState(null)
  const [euUsage, setEuUsage] = useState()
  const [duration, setDuration] = useState()

  const [totals, setTotals] = useState({
    totalEnergyCost: 0,
    totalDuration: 0,
    acceleration: 0,
    efficiencyGain: 0,
  })

  const [validations, setValidations] = useState({
    inputDataWrong: true,
    recipeEuMoreThanMachine: false,
  })

  const changeHandler = useMemo(
    () =>
      debounce((event, setter) => {
        setter(Number(event.target.value))
      }, 500),
    []
  )

  const validate = () => {
    const inputDataWrong =
      !selectedTier ||
      !euUsage ||
      !duration ||
      !Number(euUsage) ||
      !Number(duration)
    const recipeEuMoreThanMachine =
      !!selectedTier && euUsage > selectedTier.maxVoltage

    setValidations({
      ...validations,
      inputDataWrong,
      recipeEuMoreThanMachine,
    })
  }

  const calculate = async () => {
    if (Object.values(validations).some((validationValue) => validationValue)) {
      return
    }

    let initialEnergyCost = euUsage * 4
    let overclockRounds = 0

    while (initialEnergyCost <= selectedTier.maxVoltage) {
      initialEnergyCost *= 4
      overclockRounds += 1
    }

    const totalDuration = overclockRounds
      ? (duration / (overclockRounds * 2)).toFixed(2)
      : duration

    const totalEnergyCost = duration * 20 * euUsage * overclockRounds + 1 * 4

    const acceleration = (duration - totalDuration).toFixed(2)
    const efficiencyGain = (100 - (totalDuration / duration) * 100).toFixed(2)

    setTotals({
      ...totals,
      totalEnergyCost,
      totalDuration,
      acceleration,
      efficiencyGain,
    })
  }

  useEffect(() => {
    validate()
    setSelectedTier(voltageTiers[1])
  }, [])

  useEffect(() => {
    validate()
  }, [selectedTier, euUsage, duration])

  useEffect(() => {
    calculate()
  }, [validations])

  const isTierSelected = (tier) =>
    selectedTier && selectedTier.short === tier.short

  function createData(name, value) {
    return { name, value }
  }
  const rows = [
    createData(
      <ResultRowContent icon={<ElectricBolt />} name="Total Energy Cost" />,
      `${totals.totalEnergyCost} EU`
    ),
    createData(
      <ResultRowContent icon={<AvTimer />} name="Total Duration" />,
      `${totals.totalDuration} sec.`
    ),
    createData(
      <ResultRowContent icon={<AvTimer />} name="Total Acceleration" />,
      `${totals.acceleration} sec.`
    ),
    createData(
      <ResultRowContent icon={<AvTimer />} name="Efficiency Gain" />,
      `${totals.efficiencyGain}%`
    ),
  ]

  return (
    <Container maxWidth="md">
      <Grid sx={{ padding: "8px" }} container spacing={2}>
        <Grid item xs={12}>
          <Typography>Machine Voltage Tier</Typography>
          <VoltageButtonGroup>
            {voltageTiers.map((tier) => {
              return (
                <VoltageButton
                  key={tier.short}
                  isSelected={isTierSelected(tier)}
                  onClick={() => setSelectedTier(tier)}
                >
                  <Tooltip
                    title={
                      <React.Fragment>
                        <p>{tier.full}</p>
                        <Chip
                          icon={<ElectricBolt />}
                          label={<span>{tier.maxVoltage} EU/t</span>}
                          color="primary"
                        />
                      </React.Fragment>
                    }
                  >
                    <Typography>{tier.short}</Typography>
                  </Tooltip>
                </VoltageButton>
              )
            })}
          </VoltageButtonGroup>
        </Grid>
      </Grid>

      <Grid container spacing={2}>
        <Grid item xs={6}>
          <TextField
            label="EU usage"
            id="outlined-start-adornment"
            sx={{ m: 1 }}
            InputProps={{
              endAdornment: (
                <InputAdornment position="end">EU/t</InputAdornment>
              ),
              inputMode: "numeric",
              pattern: "[0-9]*",
            }}
            onInput={(event) => changeHandler(event, setEuUsage)}
          />
          <TextField
            label="Duration"
            id="outlined-start-adornment"
            sx={{ m: 1 }}
            InputProps={{
              endAdornment: (
                <InputAdornment position="end">sec.</InputAdornment>
              ),
              inputMode: "numeric",
              pattern: "[0-9]*",
            }}
            onInput={(event) => changeHandler(event, setDuration)}
          />
        </Grid>
        <Grid item xs={6}>
          <TableContainer sx={{ marginBottom: 2 }} component={Paper}>
            <Table size="small" aria-label="a dense table">
              <TableBody>
                {rows.map((row, index) => (
                  <TableRow
                    key={index}
                    sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
                  >
                    <TableCell component="th" scope="row">
                      {row.name}
                    </TableCell>
                    <TableCell align="right">{row.value}</TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
          {validations.recipeEuMoreThanMachine && (
            <Chip
              icon={<ErrorIcon />}
              label="Recipe Voltage Cannot Be Greater Than Machine Voltage"
              color="error"
            />
          )}
        </Grid>
      </Grid>
    </Container>
  )
}

export default Calculator
